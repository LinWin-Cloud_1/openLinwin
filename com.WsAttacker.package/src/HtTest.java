import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HtTest {
    public static void test() {
        System.out.print("Enter Target Url: ");
        Scanner scanner = new Scanner(System.in);
        String url = scanner.nextLine();

        System.out.print("Enter Thread Number: ");
        Scanner scanner1 = new Scanner(System.in);
        String threadNumber = scanner1.nextLine();

        System.out.print("Enter Requests Number one of the Thread: ");
        Scanner scanner2 = new Scanner(System.in);
        String requestsNumber = scanner2.nextLine();
        try{
            URL urls = new URL(url);

            for (int i = 0 ; i < Integer.valueOf(threadNumber); i++) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0 ; i < Integer.valueOf(requestsNumber) ; i++)
                        {try{
                            long s = System.currentTimeMillis();
                            HttpURLConnection httpURLConnection = (HttpURLConnection) urls.openConnection();
                            httpURLConnection.setConnectTimeout(2000);
                            httpURLConnection.setRequestMethod("GET");
                            httpURLConnection.connect();
                            InputStream inputStream = httpURLConnection.getInputStream();
                            inputStream.close();
                            httpURLConnection.disconnect();
                            long e = System.currentTimeMillis();
                            System.out.println("Target: "+url+" ; Use Time: "+(s-e));
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                        }
                    }
                });
                thread.start();
            }
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
}
